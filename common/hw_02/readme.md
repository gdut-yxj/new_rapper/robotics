# CPP 开发

1. CPP 标准 - CPP 14
2. CMake 版本 - VERSION 3.10

## 1、CMake

跟着CMake Practice 实操一遍

CMake 学习重点：

    1. 了解怎么构建一个CPP工程
    2. 了解怎么包含头文件
    3. 了解怎么将源文件编译成库或可执行文件
    3. 了解怎么使用别的库，比如 Eigen3

## 2、养成良好的代码习惯

开发过程严格按照 google 规范编写代码

https://google.github.io/styleguide/cppguide.html#Classes

开发工程可以使用 .clang-format 文件强制约束代码规范，本教程就有这个文件。

## 3、 CPP

CPP 学习重点：

    1. 了解面向对象编程，类里面public、protect、private的区别，怎么继承等
    2. 了解怎么使用多线程，资源锁
    3. 了解命名空间（namespace）的使用