# 学习使用ROS

ros 学习以官方教程为准

http://wiki.ros.org/ROS/Tutorials

跟着教程走一遍，基本的功能要熟练

学习重点：

    0. 了解ros 的 msg 的概念和使用
    1. 了解怎么使用topic
    2. 了解怎么使用service
    3. 了解怎么使用action
    4. 了解怎么使用 launch文件启动多个节点
    5. 了解怎么使用参数服务器
    6. 了解 tf 树的概念及使用

注意事项： ROS package 本质上就是一个cpp项目，也是使用CMake管理，只是 CMake部分多出一些内容。

http://wiki.ros.org/catkin/CMakeLists.txt
